// create a class
// a.) use an object to print the name of the app, sector / category, developer
// and the year it won MTN Business App of the Year Awards.
// b.) create a function inside a class, transform the app name to all capital Letters


class MTNAppOTY{

  static List<String> appDetails = [
    "Naked Insurance",
    "Financial",
    "Alex Thomson",
    "2017",
    ];

  static questionA_(){
    for (String appDetail in appDetails){
      print(appDetail);
    }
  }

  String questionB_(){
    return appDetails[0].toUpperCase();
  }

}

void main() {
  print(MTNAppOTY.questionA_());
  print(' ');
  print(MTNAppOTY().questionB_());
}