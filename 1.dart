// a basic program that stores and then prints the following data:
// your name, favourite app, and city

void main (){
  var myName = 'Mncedisi Gumede';
  var myFavouriteApp = 'Reddit';
  var myCity = 'Kempton Park, Gauteng';

  print('My name is $myName');
  print('My favourite app is $myFavouriteApp');
  print('My City is $myCity');
}