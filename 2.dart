// Create an array to store all winning apps of the MTN Business App of the Year Awards Since 2012
// Sort and print the apps by name
// print the winning app of 2017 and 2018
// print total number of apps from the array

void main() {
  List<String> appOfTheYearWinners = [
    "Awethu Project", "Health ID", "Matchy",
    "Rapidtargets", "PhraZApp", "TransUnion Dealer Guide",
    "Plascon Inspire Me", "DSTV", "Telco Data Visualizer", 
    'PriceCheck Mobile', 'MarkitShare', "Nedbank App Suite", 
    "SnapScan", "Kids Aid", "bookly",
    "Gautrain Buddy", "SuperSport", "SyncMobile", 
    "My Belongings", "LIVE Inspect", "Vigo", 'Zapper',
    "Rea Vaya", "Wildlife Tracker", "VulaMobile", 
    "DStv Now", 'WumDrop', 'CPUT Mobile', 'EskomSePush',
    'M4JAM', 'Domestly', 'iKhokha', 'HearZa', 'Tuta-me', 'KaChing'
    'Friendly Math Monsters for Kindergarten', 'MiBRAND', 'Zulzi',
    'Shyft', 'Hey Jude', 'EcoSlips', 'OrderIn', 'TransUnion 1Check',
    'InterGreatMe', 'Oru Social', 'Pick n Pay Super Animals 2', 
    'The TreeApp SA',  'Whatif Health Portal',
    'Cowa Bunga', 'Digemy Knowledge Partner and Besmarter', 'dbTrack',
    'Stokfella', 'Difela Hymns', 'Ctrl', 'snakes', 'khula ecosystem',
    'Naked Insurance', 'Digger', 'SI Realities', 'Vula Mobile', 'Hydra Farm',
    'Matric Live', 'Franc', 'LocTransi', 'LootDefense', 'MoWash', 'My Pregnancy Journey',
    'Over', 'Checkers Sixty60', 'EasyEquities', 'Technishen', 'Matric Live', 
    'Examsta', 'BirdPro', 'Lexie Hearing App', 'GreenFingers Mobile', 
    'Xitsonga Dictionary', 'StockFella', 'Bottles', 'My Pregnancy Journey',
    'Guardian Health', 'Murimi', 'Takealot.com', 'Shyft', 'Sisa', 'iiDENTIFii',
    'Hellopay SoftPOS', 'Ambani Africa', 'UniWise', 'Kazi', 'Rekindle Learning',
    'Roadsave', 'Afrihost' 
  ];
  for (String appOfTheYearWinner in appOfTheYearWinners){
    appOfTheYearWinners.sort();
    print(appOfTheYearWinner);
  }
  print('');
  print(appOfTheYearWinners[47] + ' 2017 App of the Year Winner');
  print('');
  print(appOfTheYearWinners[90] + ' 2018 App of The Year Winner');
  print('');
  print('Total Number of Apps: ' + appOfTheYearWinners.length.toString());
}